#!/usr/bin/python3
from shutil import copyfile
from shutil import move
from os import fdopen, remove
from tempfile import mkstemp
import subprocess
import re

def replace_in_file(file_path, pattern, replacement):
	fh, abs_path = mkstemp()
	with fdopen(fh, 'w') as new_file:
		with open(file_path) as old_file:
			for line in old_file:
				new_file.write(line.replace(pattern, replacement))
	remove(file_path)
	move(abs_path, file_path)


def exec_command(command):
	print(subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read())


def add_to_file(file_name, content):
	with open(file_name, 'a') as file:
		file.write(content)


def backup_and_add(folder, file_name, line_to_add):
	file_to_act_on = "{}{}".format(folder, file_name)
	backup_file = "{}backup.{}".format(folder, file_name)
	
	print("Backup file {} to {}".format(file_to_act_on, backup_file))
	copyfile(file_to_act_on, backup_file)

	print("Append to file: {}".format(line_to_add))
	add_to_file("{}{}".format(folder, file_name), line_to_add)

	print("")


def rev_serial(file_name, update=True):
	line_str = None
	serial = 0

	regex = re.compile("([0-9]+).*Serial")
	with open(file_name) as file:
		for line in file:
			result = regex.search(line)
			if result is not None:
				line_str = result.string
				serial = result.groups(0)[0]
				break
	
	if line_str is not None:
		new_serial = str(int(serial)+1)
		new_line_str = line_str.replace(serial, new_serial)
	
		if update:	
			print("Incrementing serial on {} from {} to {}.".format(file_name, serial, new_serial))
			replace_in_file(file_name, line_str, new_line_str)
		else:
			print("Next serial for {} is {}.".format(file_name, new_serial))	
		
		return int(new_serial)
	else:
		raise Exception("Serial was not found in {} but was expected.".format(file_name))

next_ip = 1
environment_path = None
environment_type = input("Dev/Test/Prod: ").lower()

if environment_type == "dev":
	environment_path = "/etc/dhcp/current_dev_ip"
	next_ip = rev_serial(environment_path, False)
	if next_ip >= 200:
		raise Exception("All out of dev IPs (100-199 only)")

elif environment_type == "test":
	environment_path = "/etc/dhcp/current_test_ip"
	next_ip = rev_serial(environment_path, False)
	if next_ip >= 100:
		raise Exception("All out of test IPs (50-99 only)")

elif environment_type == "prod":
	environment_path = "/etc/dhcp/current_prod_ip"
	next_ip = rev_serial(environment_path, False)
	if next_ip >=240:
		raise Exception("All out of prod IPs (200-239 only)")
else:
	raise Exception("Environment type must be dev, test or prod.")

# Find out what the next IP address will be...
next_ip = rev_serial(environment_path, False)
ip_address = "10.1.32.{}".format(str(next_ip))

print ("IP Address (e.g. 10.1.32.201): {}".format(ip_address))

mac_address = input("MAC Address (e.g. 00:00:00:00:00:00): ")
host_name = input ("Computer Name (e.g. vsrvsql): ")

ip_host = ip_address[::-1].split('.')[0][::-1]
fqdn = "{}.internal.abuvmedia.com".format(host_name);

folder="/etc/bind/zones/"
file_name = "db.internal.abuvmedia.com"
line_to_add = "{}.	IN	A	{}\n".format(fqdn, ip_address)
backup_and_add(folder, file_name, line_to_add)
rev_serial("{}{}".format(folder, file_name))

file_name = "db.10.1.32"
line_to_add = "{}	IN	PTR	{}. ; {}\n".format(ip_host, fqdn, ip_address)
backup_and_add(folder, file_name, line_to_add)
rev_serial("{}{}".format(folder, file_name))

folder="/etc/dhcp/"
file_name = "dhcp-assignments.conf"
line_to_add = "host {} {{ hardware ethernet {}; fixed-address {}; }}\n".format(host_name, mac_address, ip_address)
backup_and_add(folder, file_name, line_to_add)

# To make sure that we don't allocate the same IP address next time, rev the serial for the environment.
rev_serial(environment_path)

exec_command("chown bind:bind /etc/bind/zones/*")
exec_command("named-checkzone 10.1.32 /etc/bind/zones/db.10.1.32")
exec_command("named-checkzone internal.abuvmedia.com /etc/bind/zones/db.internal.abuvmedia.com")
exec_command("rndc reload")
exec_command("service isc-dhcp-server restart")
exec_command("service bind9 restart")

