#!/usr/bin/python3
import subprocess


def exec_command(command):
	print(subprocess.Popen(command, shell=True, stdout=subprocess.PIPE).stdout.read())


exec_command("named-checkzone 10.1.32 /etc/bind/zones/db.10.1.32")
exec_command("named-checkzone internal.abuvmedia.com /etc/bind/zones/db.internal.abuvmedia.com")
exec_command("rndc reload")
exec_command("service isc-dhcp-server restart")
exec_command("service bind9 restart")

